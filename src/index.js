import React, {createContext, StrictMode} from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import UserStore from './stores/UserStore';
import QuizStore from "./stores/QuizStore";
import GlobalQuizzesStore from "./stores/GlobalQuizzesStore";
import reportWebVitals from './reportWebVitals';

// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";
// Bootstrap Bundle JS
import "bootstrap/dist/js/bootstrap.bundle.min";

export const Context = createContext(null)
const root = ReactDOM.createRoot(    document.getElementById('root'))
root.render(
    <StrictMode>
        <Context.Provider value ={{
            user: new UserStore(),
            quiz: new QuizStore(),
            globalQuizzes: new GlobalQuizzesStore()
        }}>
            <App />
        </Context.Provider>
    </StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
