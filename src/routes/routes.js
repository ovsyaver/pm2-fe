import {
    ANSWER_ROUTE,
    AUTH_ROUTE,
    MAIN_ROUTE,
    MYQUIZ_ROUTE,
    NEWQUIZ_ROUTE,
    QUIZ_ROUTE,
    STAT_ROUTE,
    USER_ROUTE
} from "../utils/consts";
import {Auth} from "../pages/Auth";
import {User} from "../pages/User";
import Quiz from "../pages/Quiz";
import {Main} from "../pages/Main";
import MyQuiz from "../pages/MyQuiz";
import Stats from "../pages/Stats";
import NewQuiz from "../pages/NewQuiz";
import Answer from "../pages/Answer";

export const authRoutes = [
    {
        path: USER_ROUTE,
        Component: User
    },
    {
        path: QUIZ_ROUTE + '/:id',
        Component: Quiz
    },
    {
      path: NEWQUIZ_ROUTE + '/:id?',
      Component: NewQuiz
    },
    {
        path: MYQUIZ_ROUTE,
        Component: MyQuiz
    },
    {
        path: STAT_ROUTE,
        Component: Stats
    },
    {
        path: MAIN_ROUTE,
        Component: Main
    },
    {
        path: ANSWER_ROUTE + '/:id',
        Component: Answer
    }
]

export const publicRoutes = [
    {
        path: AUTH_ROUTE,
        Component: Auth
    }
]