import React, {useContext, useEffect, useState} from 'react';
import {Container} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import QuizService from "../service/quiz.service";
import {Cell, Legend, Pie, PieChart} from "recharts";

const Stats = observer(() => {

    const {user} = useContext(Context);
    const [stats,setStats] = useState([]);
    const [avg,setAvg] = useState(0);
    const [totalQuizzes,setTotalQuizzes] = useState(0);
    const COLORS = ['#A895C5', '#96D1A6', '#FFC94D', '#6BB1D4', '#FF6B6B'];




    function chartManage(gradesMap) {
        const gradesGroupedArr = [];
        for (const gradesElement of gradesMap) {
            let groupName = gradesElement[0];
            let upToLine = groupName + 20;
            const nameString = "from " + groupName + " to " + upToLine + " %";
            gradesGroupedArr.push({
                "name": nameString,
                "value": gradesElement[1]
            })
        }
        return gradesGroupedArr;
    }

    function procGapManage(grade) {
        const item = grade;
        for (let lowEnd = 0; lowEnd < 120; lowEnd += 20) {
            const highEnd = lowEnd + 20;
            if (item >= lowEnd && item <= highEnd){
                if (lowEnd < 100){
                    return lowEnd;
                } else {
                    return 100;
                }
            }
        }
    }
    function gapManage(grade) {
        const item = grade;
        for (let lowEnd = 0; lowEnd < 20; lowEnd += 3) {
            const highEnd = lowEnd + 3;
            if (item >= lowEnd && item <= highEnd){
                if (lowEnd < 17){
                    return lowEnd;
                } else {
                    return 100;
                }
            }
        }
    }

    function mapCreate(grades, gradesMap) {
        for (const grade of grades) {
            const gradeGapKey = procGapManage(grade);
            if (gradesMap.has(gradeGapKey)) {
                let value = gradesMap.get(gradeGapKey);
                gradesMap.set(gradeGapKey, ++value);
            } else {
                gradesMap.set(gradeGapKey, 1);
            }
        }
    }

    function groupGrades(grades) {
        const gradesMap = new Map();
        mapCreate(grades, gradesMap);
        return chartManage(gradesMap);
    }

    useEffect(() => {
        QuizService.fetchStats(user.token).then(({data}) => {
            console.log(data);
            setTotalQuizzes(data.grades.length);
            setStats(groupGrades(data.grades));
            setAvg(data.averageGrade.toFixed(2));
        });
    }, [user]);

    return (
        <Container className='content-block min-vh-100 mt-1'>
            <h1>Your Statistics</h1>
            <h5 className={totalQuizzes > 0 ? 'd-block' : 'd-none'}>You have solved {totalQuizzes} {totalQuizzes > 1 ? 'quizzes' : 'quiz'}</h5>
            <h2>Your score chart:</h2>
            <PieChart width={800} height={400}>
                <Legend iconSize={10} layout="vertical" verticalAlign="middle"/>
                <Pie
                    className='ml-2'
                    data={stats}
                    cx={120}
                    cy={200}
                    innerRadius={40}
                    outerRadius={80}
                    fill="#8884d8"
                    paddingAngle={5}
                    dataKey="value"
                    label
                >
                    {stats.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                </Pie>
            </PieChart>
            <h2>Your average score: {avg}%</h2>
        </Container>
    );
});

export default Stats;