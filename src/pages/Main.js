import React, {useContext} from 'react';
import Row from "react-bootstrap/Row";
import {Context} from "../index";
import {GlobalQuizzesList} from "../components/GlobalQuizzesList";
import {Container, Modal, Spinner} from "react-bootstrap";


export const Main = () => {

    return (
        <Container className="content-block min-vh-100 mt-1">
            <h1>Global Quizzes</h1>
            <Row>
                <GlobalQuizzesList/>
            </Row>
        </Container>
    );
};