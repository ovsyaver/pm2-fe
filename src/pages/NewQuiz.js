import React from 'react';
import {Container} from "react-bootstrap";
import {AddQuizForm} from "../components/AddQuizForm";

const NewQuiz = () => {
    return (
        <Container className='content-block min-vh-100 mt-1'>
            <AddQuizForm/>
        </Container>
    );
};

export default NewQuiz;