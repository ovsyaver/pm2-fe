import React, {useContext, useEffect, useReducer, useState} from 'react';
import {Button, Container} from "react-bootstrap";
import {Context} from "../index";
import QuizService from "../service/quiz.service";
import UserService from "../service/user.service";
import {ANSWER_ROUTE, NEWQUIZ_ROUTE} from "../utils/consts";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import {observer} from "mobx-react-lite";



const MyQuiz = observer(() => {
    const { user } = useContext(Context);
    const [userQuizzes, setUserQuizzes] = useState([]);
    const [solvedUserQuizzes, setSolvedUserQuizzes] = useState([]);
    const [menuToggle, setMenuToggle] = useState(true);
    const history = useHistory();

    const fetchData = async () => {
        try {
            if (menuToggle) {
                const resp = await QuizService.fetchMyQuizzes(user.token);
                setUserQuizzes(resp.data);
            } else {
                const resp = await QuizService.fetchSolvedQuizzes(user.token);
                setSolvedUserQuizzes(resp.data);
            }
        } catch (e) {
            console.log(e);
        }
    };

    const deleteQuiz = async (id) => {
        try {
            await QuizService.deleteQuiz(id, user.token);
            fetchData(); // Обновляем список после удаления
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        fetchData();
    }, [menuToggle]);


    return (
        <Container className='content-block min-vh-100 mt-1'>
            <h1>My Quizzes</h1>
            <div>
                <span className='d-flex justify-content-around'>
                    <Button className={menuToggle ? "btn active btn-outline-primary mt-2" : "btn btn-outline-primary mt-2"} style={{cursor: "pointer"}} onClick={event => setMenuToggle(true)}>Created</Button>
                    <Button className={!menuToggle ? "btn active btn-outline-primary mt-2" : "btn btn-outline-primary mt-2"} style={{cursor: "pointer"}} onClick={event => setMenuToggle(false)}>Solved</Button>
                </span>
            </div>
            <div>
                { menuToggle ?
                        <div>
                            {(() => {
                                if (userQuizzes && userQuizzes.length > 0) {
                                    return (
                                        userQuizzes.map(quiz =>
                                            <div key={quiz.id} className='m-2 border-bottom column'>
                                                <div className='row justify-content-between ml-2 cursor-pointer'>
                                                    {quiz.name}
                                                    <div className=''>
                                                        <button className='btn' onClick={() => history.push(NEWQUIZ_ROUTE + '/' + quiz.id)}>✏️</button>
                                                        <button className='btn mr-2' onClick={() => deleteQuiz(quiz.id)}>✖</button>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    )
                                }
                            }) ()}
                        </div>
                    :
                        <div>
                            {(() => {
                                if (solvedUserQuizzes.length > 0) {
                                    return (
                                        solvedUserQuizzes.map(quiz =>
                                            <div key={quiz.id} className='m-2 border-bottom column mt-3' onClick={() => history.push(ANSWER_ROUTE + '/' + quiz.id)}>
                                                <div className='row justify-content-between'>
                                                    <div className='ml-4 cursor-pointer'>
                                                        {quiz.name}
                                                    </div>
                                                    <div className='mr-4'>
                                                        Points awarded: {quiz.points} / {quiz.maxQuizPoints}
                                                    </div>

                                                </div>
                                            </div>
                                        )
                                    )
                                }
                            }) ()}
                        </div>
                }
            </div>
        </Container>
    );
});

export default MyQuiz;