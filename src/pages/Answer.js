import React, {useContext, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite";
import {Card, Col, Container} from "react-bootstrap";
import {QuestionItem} from "../components/QuestionItem";
import QuizService from "../service/quiz.service";
import {Context} from "../index";
import {useParams} from "react-router-dom";
import {SINGLE_RIGHT, TEXT} from "../utils/consts";

const Answer = observer(() => {

    const {quiz, user} = useContext(Context)
    const [quizName, setQuizName] = useState("")
    const [result, setResult] = useState({})
    const [points, setPoints] = useState(0)
    const [quizMaxPoints, setQuizMaxPoints] = useState(0)
    const {id} = useParams()

    useEffect( () => {
        QuizService.fetchAnswer(id, user.token).then(({data}) => {
            console.log(data);
            setPoints(data.points);
            setQuizName(data.quizName);
            setQuizMaxPoints(data.quizMaxPoints);
            setResult(JSON.parse(data.result));
            console.log(result);
        })
    }, []);

    return (
        <Container className="content-block min-vh-100 mt-1">
            <h1>{quizName}</h1>
            <h5>Your points: {points} / {quizMaxPoints}</h5>
            <Col>
                {result && result.length > 0 ? (
                    result.map((question) => (
                        <Card key={question.id} className='question-body mt-3'>
                            <Col>
                                <div className='row justify-content-between'>
                                    <div className='col-3'>{question.type}</div>
                                    <div className='col-2'>{question.maxQuestionPoints} {question.maxQuestionPoints > 1 ? 'points' : 'point'}</div>
                                </div>
                                <div className='row ml-1 mt-2 justify-content-between'>
                                    <h4>{question.text}</h4>
                                    {question.answerCorrect ?
                                        <h4 className='mr-4'>✅</h4>
                                        :
                                        <h4 className='mr-4'>❌</h4>
                                    }
                                </div>
                            </Col>
                            <Col>
                                {question.type !== TEXT ?
                                    question.answerOptions.map(answer =>
                                        <Col key={answer.id}>
                                            {question.type === SINGLE_RIGHT ?
                                                <input type="radio" value={answer.id} name={answer.id} id={answer.id} checked={question.answer == answer.id}/>
                                                :
                                                <input type="checkbox" value={answer.id} name={answer.id} id={answer.id} checked={question.answer.split(',').includes(answer.id)}/>
                                            }
                                            <label className='ml-2' htmlFor={answer.id}>{answer.text}</label>
                                        </Col>
                                    )
                                    :
                                    <input type="text" value={question.answer} className='mb-2'/>
                                }
                                {!question.answerCorrect ?
                                    <p>Correct answer: {question.answerOptions.map(answer => {
                                        if (answer.correct){
                                            return answer.text + " "
                                        }
                                    })}</p>
                                    :
                                    <p>Amazing, you got it!</p>
                                }
                            </Col>
                        </Card>
                    ))
                ) : (
                    <p>No questions available</p>
                )}
            </Col>
        </Container>
    );
});

export default Answer;