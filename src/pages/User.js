import React, {useContext, useEffect, useState} from 'react';
import {Button, Col, Container, Form} from "react-bootstrap";
import {Context} from "../index";
import UserService from "../service/user.service";
import {AUTH_ROUTE, MAIN_ROUTE, USER_ROUTE} from "../utils/consts";
import {useHistory} from "react-router-dom/cjs/react-router-dom";

export const User = () => {

    const {user} = useContext(Context);
    const history = useHistory();
    const [newPwd, setNewPwd] = useState('');
    const [confNewPwd, setConfNewPwd] = useState('');
    const PWD_REGEX = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    const [validPwd, setValidPwd] = useState(false);
    const [validPwdMatch, setValidPwdMatch] = useState(false);

    useEffect(() => {
        const pwdTest = PWD_REGEX.test(newPwd)
        setValidPwd(pwdTest)
        const match = newPwd === confNewPwd
        setValidPwdMatch(match)
    }, [newPwd, confNewPwd])

    const changePassword = () => {
        if (validPwd && validPwdMatch){
            UserService.changePassword(user.id, newPwd, user.token).then(res=>{
                try {
                    console.log(res.data)
                    alert('Your password successfully changed');
                }catch (err){
                    console.log(err.response.data.message)
                }
            })
        }
    }

    const deleteUser = () => {
        UserService.deleteAccount(user.id, user.token).then(res=>{
            try {
                user.setUser({})
                user.setIsAuth(false)
                history.push(AUTH_ROUTE)
            }catch (err){
                console.log(err)
            }
        })
    }


    return (
        <Container className="content-block min-vh-100 mt-1">
            <div>
                <div>
                    <h1>Account</h1>
                </div>
                <div className='container mt-4' id='user-data'>
                    <div className='row'>
                        <h2>Your account information:</h2>
                    </div>
                    <div>
                        <p>Username: {user.username}</p>
                        <p>Email: {user.email}</p>
                    </div>
                    <div className='row mt-4'>
                        <h2>Change password:</h2>
                    </div>
                </div>

                <div className='container'>
                    <Form className='row'>
                        <Form.Group className='password-setter'>
                            <Form.Control
                                placeholder="choose new password"
                                value={newPwd}
                                onChange={(e) => setNewPwd(e.target.value)}
                                type="password"
                                className='mt-2 mb-1'
                            />
                            {/*Make this notification interactive*/}
                            <p className={(newPwd.length > 0 && !validPwdMatch) ? "instructions" : "hide"}>
                                At least 8 characters, 1 letter and 1 number
                            </p>

                            <Form.Control
                                placeholder="confirm new password"
                                value={confNewPwd}
                                onChange={(e) => setConfNewPwd(e.target.value)}
                                type="password"
                                className='mt-2'
                            />
                            <p className={(confNewPwd.length > 0 && !validPwdMatch) ? "instructions" : "hide"}>
                                Must match the password above
                            </p>
                            <Button className='mt-3' variant='btn btn-primary' onClick={changePassword}>
                                Set Password
                            </Button>
                        </Form.Group>
                    </Form>
                </div>
                <div className='container pl-0'>
                    <Button className='mt-5' variant='btn btn-outline-primary' onClick={deleteUser}>
                        Delete Account
                    </Button>
                </div>
            </div>
        </Container>
    );
};