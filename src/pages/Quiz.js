import React, {useState, useEffect, useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Alert, Button, Card, Col, Container, Modal} from "react-bootstrap";
import QuizService from "../service/quiz.service";
import {useParams} from "react-router-dom";
import Row from "react-bootstrap/Row";
import {QuestionItem} from "../components/QuestionItem";
import {Context} from "../index";
import {set} from "mobx";
import {ANSWER_ROUTE, QUIZ_ROUTE} from "../utils/consts";
import {useHistory} from "react-router-dom/cjs/react-router-dom";

const Quiz = observer(() => {

    const {quiz, user} = useContext(Context)
    const [totalPoints, setTotalPoints] = useState(0);
    const [complete, setComplete] = useState(false);
    const {id} = useParams()
    const history = useHistory();

    useEffect( () => {
        QuizService.fetchQuiz(id, user.token).then(({data}) => {
            console.log(data)
            quiz.setAnswers(new Map)
            quiz.setId(data.id)
            quiz.setAuthorId(data.authorId)
            quiz.setName(data.name)
            quiz.setQuestions(data.questions)
            quiz.setQuiz(quiz)
            console.log(quiz)
            let score = 0
            for(let question of quiz.questions){
                score += question.points;
            }
            setTotalPoints(score);
        })
    }, [totalPoints]);

    const endAndSend = () => {
        if (quiz.answers.size === quiz.questions.length){
            if (!complete){
                let answersToJson = [];
                quiz.answers.forEach(function (value, key) {
                    answersToJson.push({
                        questionId: key,
                        answer: value.toString()
                    })
                })
                QuizService.sendQuiz(quiz.id, user.token, answersToJson).then(({data}) => {
                    setComplete(true);
                    history.push(ANSWER_ROUTE + '/' + data);
                })
            }
        } else {
            document.getElementById('uncomplete-quiz-warning').className = 'd-block'
        }
    }

    return (
        <Container className="content-block min-vh-100 mt-1">
            <Alert id='uncomplete-quiz-warning' variant='danger' className='d-none'>Please answer every question</Alert>
            <h1>{quiz.name}</h1>
                <h5>Total points: {totalPoints}</h5>
            <Button onClick={endAndSend} className='mb-3'>
                End and send
            </Button>
            <Col>
                {quiz.questions && quiz.questions.length > 0 ? (
                    quiz.questions.map((question) => (
                        <QuestionItem key={question.id} question={question} />
                    ))
                ) : (
                    <p>No questions available</p>
                )}
            </Col>
            <Button onClick={endAndSend}>
                End and send
            </Button>
        </Container>
    );
});

export default Quiz;