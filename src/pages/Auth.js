import {useState, useEffect, useContext} from "react";
import UserService from '../service/user.service'
import {LogUser} from "../models/logUser";
import {RegUser} from "../models/regUser";
import {Context} from "../index";
import {MAIN_ROUTE, USER_ROUTE} from "../utils/consts";
import Cookies from 'js-cookie';
import {Form, Button, Card, Container, Image} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import {useHistory} from "react-router-dom/cjs/react-router-dom";

const USER_REGEX = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,15}$/;
const PWD_REGEX = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
const EMAIL_REGEX = /^[\w-]+@([\w-]+\.)+[\w-]{2,4}$/
export const Auth = () => {

    const {user} = useContext(Context)

    const history  = useHistory()

    const [isLogin, setIsLogin] = useState(false);

    const [username, setUsername] = useState('');
    const [usernameFocus, setUsernameFocus] = useState(false);

    const [pwd, setPwd] = useState('');
    const [pwdFocus, setPwdFocus] = useState(false);

    const [email, setEmail] = useState('');
    const [emailFocus, setEmailFocus] = useState(false);

    const [confPwd, setConfPwd] = useState('');
    const [confPwdFocus, setConfPwdFocus] = useState(false);

    const [validUser, setValidUser] = useState(false);
    const [validEmail, setValidEmail] = useState(false);
    const [validPwd, setValidPwd] = useState(false);
    const [validPwdMatch, setValidPwdMatch] = useState(false);

    const [success, setSuccess] = useState(false);
    const [errMsg, setErrMsg] = useState('');

    useEffect(() => {
        const userTest = USER_REGEX.test(username)
        setValidUser(userTest)
    }, [username])

    useEffect(() => {
        const pwdTest = PWD_REGEX.test(pwd)
        setValidPwd(pwdTest)
        const match = pwd === confPwd
        setValidPwdMatch(match)
    }, [pwd, confPwd])

    useEffect(() => {
        const emailTest = EMAIL_REGEX.test(email)
        setValidEmail(emailTest)
    }, [email])

    useEffect(() => {
        setErrMsg('');
    }, [username, email, pwd, validPwdMatch])

    const register = () => {
        UserService.register(new RegUser(username, email, pwd)).then(res=>{
            try {
                if (!success) {
                    setSuccess(true)
                    user.setId(res.data.id)
                    user.setEmail(res.data.email)
                    user.setToken(res.data.token)
                    user.setUsername(res.data.name)
                    user.setUser(user)
                    user.setIsAuth(true)
                    sessionStorage.setItem('id', user.id);
                    sessionStorage.setItem('token', user.token);
                    sessionStorage.setItem('email', user.email);
                    sessionStorage.setItem('name', user.username);
                    history.push(MAIN_ROUTE)
                }
            }catch (err){
                document.getElementById('error').className = 'errorMsg border-danger'
                setErrMsg('Something went wrong')
                setSuccess(false)
            }
        })
    }

    const login = () => {
        UserService.login(new LogUser(email, pwd)).then(res=>{
            try {
                if (!success) {
                    setSuccess(true)
                    user.setId(res.data.id)
                    user.setEmail(res.data.email)
                    user.setUsername(res.data.name)
                    user.setToken(res.data.token)
                    user.setUser(user)
                    user.setIsAuth(true)
                    sessionStorage.setItem('id', user.id);
                    sessionStorage.setItem('token', user.token);
                    sessionStorage.setItem('email', user.email);
                    sessionStorage.setItem('name', user.username);
                    history.push(MAIN_ROUTE)
                }
            }catch (err){
                document.getElementById('error').className = 'errorMsg border-danger'
                setErrMsg('Login or password are wrong')
                setSuccess(false)
            }
        })
    }


    const submit = () => {
        if (isLogin) {
            login();
        } else {
            register();
        }
    }


    return (
        <Container id="auth" className='min-vw-100 min-vh-100'>
            <Card className='content-block'>
                <Image src={'logo_Qwiz_Wiz.png'}/>
                <h2 className="m-auto font-weight-bold">{isLogin ? 'Login' : "Sign up"}</h2>
                <p id="error" className="errorMsg border-danger d-none" onClick={ event => {
                    document.getElementById('error').className = 'errorMsg border-danger d-none'
                }}>{errMsg}</p>
                <Form className="d-flex flex-column">
                    { isLogin ?
                        <Form.Group>
                            <Form.Control
                                className='mt-3 mb-3'
                                placeholder="username or email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            <Form.Control
                                className='mt-3 mb-3'
                                placeholder="your password"
                                value={pwd}
                                onChange={e => setPwd(e.target.value)}
                                type="password"
                                required
                            />
                        </Form.Group>
                        :
                        <Form.Group>
                            <Form.Control
                                placeholder="choose an username"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                onFocus={() => setUsernameFocus(true)}
                                onBlur={() => setUsernameFocus(false)}
                                required
                            />
                            <p className={usernameFocus && !validUser ? "instructions" : "hide"}>
                                Username must be 2 to 15 characters.<br />
                            </p>
                            <Form.Control
                                placeholder="type your email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                onFocus={() => setEmailFocus(true)}
                                onBlur={() => setEmailFocus(false)}
                                aria-invalid={validEmail ? "false" : "true"}
                                required
                            />
                            <p className={emailFocus && !validEmail ? "instructions" : "hide"}>
                                Valid email example : email@domen.com
                            </p>
                            <Form.Control
                                placeholder="choose a password"
                                value={pwd}
                                onChange={(e) => setPwd(e.target.value)}
                                onFocus={() => setPwdFocus(true)}
                                onBlur={() => setPwdFocus(false)}
                                type="password"
                            />
                            <p className={pwdFocus && !validPwd ? "instructions" : "hide"}>
                                At least 8 characters, 1 letter and 1 number
                            </p>
                            <Form.Control
                                className= {confPwdFocus && !validPwdMatch ? "password invalidEle" : "password ele"}
                                placeholder="confirm the password"
                                value={confPwd}
                                onChange={(e) => setConfPwd(e.target.value)}
                                onFocus={() => setConfPwdFocus(true)}
                                onBlur={() => setConfPwdFocus(false)}
                                type="password"
                            />
                            <p className={confPwdFocus && !validPwdMatch ? "instructions" : "hide"}>
                                Must match the password above
                            </p>
                        </Form.Group>
                    }
                    <Row className="d-flex justify-content-between mt-3 pl-3 pr-3 mt-4">

                        <Button variant="btn btn-primary" onClick={submit}>
                            {isLogin ? 'Login' : 'Sign up'}
                        </Button>
                        <Button className="btn btn-outline-primary mt-2" style={{cursor: "pointer"}} onClick={() => {
                            setIsLogin(!isLogin);
                            setEmail('');
                            setUsername('');
                            setPwd('');
                            setConfPwd('');
                            document.getElementById('error').className = 'errorMsg border-danger d-none';
                        }}>
                            {isLogin ?
                                'Don\'t have an account?'
                                :
                                'Already have an account?'
                            }
                        </Button>
                    </Row>

                </Form>
            </Card>
        </Container>
    )
}