import React, {useContext, useEffect, useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {QuestionForm} from "./QuestionForm";
import {observer} from "mobx-react-lite";
import QuizService from "../service/quiz.service";
import {AddQuiz} from "../models/addQuiz";
import {Context} from "../index";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import {MAIN_ROUTE, MULTIPLE_RIGHT, SINGLE_RIGHT, TEXT} from "../utils/consts";
import {useParams} from "react-router-dom";
import {updateQuiz} from "../models/updQuiz";

export const AddQuizForm = observer(() => {

    const {user, quiz} = useContext(Context)
    const history = useHistory();
    const {id} = useParams();
    const [errMsg, setErrMsg] = useState('')
    const [quizName, setQuizName] = useState('');
    const [questionCount, setQuestionCount] = useState(0)
    const [questions, setQuestions] = useState([])


    const publish = () => {
        if (id === undefined) {
            QuizService.create(new AddQuiz(quizName, questions), user.token).then(r => {
                try {
                    console.log('Quiz created by ' + user.username);
                    history.push(MAIN_ROUTE);
                } catch (e) {
                    setErrMsg(e.response.data.message);
                }
            })
        } else {
            QuizService.update(new updateQuiz(quiz.id, quizName, questions), user.token).then(r => {
                try {
                    console.log('Quiz created by ' + user.username);
                    history.push(MAIN_ROUTE);
                } catch (e) {
                    setErrMsg(e.response.data.message);
                }
            })
        }
    }

    const addQuestion = () => {
        setQuestions(prevState => [...prevState, {
            type: null,
            text: '',
            points: 1,
            answerOptions: []
        }])
        setQuestionCount(prevState => prevState + 1);
    }

    useEffect(() => {
        if (id !== undefined) {
            QuizService.fetchQuiz(id, user.token).then(({data}) => {
                quiz.setId(data.id);
                quiz.setName(data.name);
                quiz.setQuestions(data.questions);
                setQuizName(data.name);
                setQuestions(data.questions);
                console.log(quiz)
            });

        }

    }, [id]);

    return (
        <Form className='quiz-form mb-3'>
            <h1 className='pb-2'>Create new Quiz</h1>
            <p className={errMsg.length > 0 ? "errorMsg" : "d-none"}>{errMsg}</p>
            <div className='new-quiz-form-body'>
                <div id='quiz-info' className='quiz-form-item'>
                    <Form.Control placeholder='Enter quiz name' id='quiz-name' type="text" value={quizName} onChange={e => setQuizName(e.target.value)}/>
                    <div className='quiz-controls mt-2 pb-3'>
                        <button onClick={() => history.push(MAIN_ROUTE)} className='btn btn-outline-primary float-right'>Cancel</button>
                        <button onClick={() => publish()} className='btn btn-primary float-right mr-2'>Save and Publish</button>
                    </div>
                </div>
                <div>
                    <ul id='quiz-questions'>
                        {questions.map(question =>
                            <li key={question.index}>
                                <QuestionForm question={question}/>
                            </li>
                        )}
                    </ul>
                    <button className='btn-btn plus btn btn-primary' onClick={addQuestion}>+</button>
                </div>
            </div>
        </Form>
    );
});