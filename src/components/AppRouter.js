import React, {useContext, useEffect, useState} from 'react';
import {Route, Switch} from 'react-router-dom'
import {observer} from "mobx-react-lite";
import {Context} from "../index.js";
import {authRoutes, publicRoutes} from "../routes/routes";
import {Redirect} from "react-router-dom";
import {AUTH_ROUTE, MAIN_ROUTE} from "../utils/consts";
import {useHistory} from "react-router-dom/cjs/react-router-dom";

const AppRouter = observer(() => {
    const {user} = useContext(Context);
    const history = useHistory();

    useEffect(() => {
        if (sessionStorage.getItem('token')){
            user.setId(sessionStorage.getItem('id'));
            user.setEmail(sessionStorage.getItem('email'));
            user.setToken(sessionStorage.getItem('token'));
            user.setUsername(sessionStorage.getItem('name'));
            user.setIsAuth(true);
            history.push(MAIN_ROUTE);
        }
    }, []);


    return (
        <Switch>
            {user.isAuth && authRoutes.map(({path, Component}) =>
                <Route key={path} path={path} component={Component} exact/>
            )}
            {publicRoutes.map(({path, Component}) =>
                <Route key={path} path={path} component={Component} exact/>
            )}
            <Redirect to={AUTH_ROUTE}/>
        </Switch>
    );
});

export default AppRouter;