import {observer} from "mobx-react-lite";
import {Button, Card, Col} from "react-bootstrap";
import {MULTIPLE_RIGHT, SINGLE_RIGHT, TEXT} from "../utils/consts";
import React, {useEffect, useState} from "react";

export const QuestionForm = observer((props) => {

    const question = props.question;
    const types = [{text: 'Text Question', value:TEXT}, {text: 'Single Right Choice', value:SINGLE_RIGHT}, {text: 'Multiple Right Choices', value:MULTIPLE_RIGHT}]
    const [selectedType, setSelectedType] = useState(TEXT);
    const [inputValue, setInputValue] = useState(question.text);
    const [points, setPoints] = useState(question.points)
    const [rightSelected, setRightSelected] = useState('');
    const [answers, setAnswers] = useState(question.answerOptions);
    const [answerCount, setAnswerCount] = useState(question.answerOptions.length);

    function addAnswer() {
        question.answerOptions.push({
            isCorrect: false,
            text: ''
        });
        setAnswers(question.answerOptions);
        setAnswerCount(prevState => prevState + 1)
    }

    function handleChange(index, target) {
        if (question.answerOptions?.length === 0 && question.type === TEXT) {
            question.answerOptions = []
            question.answerOptions.push({
                isCorrect: true,
                text: target.value
            });
        } else {
            let newAnswers;
            let text = target.value;
            let correct = target.checked;
            if (answers.length > 0) {
                newAnswers = answers.map((v, i) => {
                    if (i === index) {
                        return {isCorrect: correct, text: text};
                    } else {
                        return v;
                    }
                });
            } else {
                newAnswers = [{
                    isCorrect: true,
                    text: target.value
                }];
            }
            setAnswers(newAnswers);
            question.answerOptions = newAnswers;
        }
        let rightOptions = question.answerOptions.filter(option => option.isCorrect).length;
        let err = "";
        switch (question.type) {
            case TEXT:
                if (question.answerOptions.length !== 1) {
                    err ="Text Question must have only one answer option";
                }
                if (rightOptions !== 1) {
                    err ="Text Question must have one correct answer option";
                }
                break;
            case SINGLE_RIGHT:
                if (rightOptions !== 1) {
                    err = "Single Right Choice question must have one correct answer option";
                }
                break;
            case MULTIPLE_RIGHT:
                if (rightOptions <= 1) {
                    err ="Multiple Right Choices question must have more than one correct answer option";
                }
                break;
        }
        if (err !== "") {
            document.getElementById('popup').style.right = '0';
            document.getElementById('popup').innerText = err;
            setTimeout(()=> { if (document.getElementById('popup')) {
                document.getElementById('popup').style.right = '-3000px';
                document.getElementById('popup').innerText = '';
            }}, 5000);
        }
    }

    function deleteAnswer(index) {
        if (question.answerOptions.length > index
                && question.answerOptions[index] !== undefined){
            delete question.answerOptions[index];
        }
        question.answerOptions = question.answerOptions.filter((answer) => answer !== undefined);
        setAnswers(question.answerOptions);
        setAnswerCount(prevState => prevState - 1)
    }

    useEffect(() => {
        if (question.type !== null) {
            setSelectedType(question.type)
        } else {
            setSelectedType(TEXT);
            question.type = TEXT;
        }
    }, [question])

    return (
        <Card className='question-body d-flex flex-column align-items-center mt-5'>
            <div className='container mt-2'>
                <select name="question-type" id="q-type" className='mr-3 question-elem'
                        onChange={event => {
                            setSelectedType(event.target.value)
                            question.type = event.target.value;
                        }}>
                    {types.map((type, index) =>
                        <option key={index} value={type.value}
                                selected={question.type === type.value}>{type.text}</option>
                    )}
                </select>
                <label className="mr-2" htmlFor="points">Points:</label>
                <input id='q-points' name='points' type="number" min='1' max='100' value={points}
                       className='question-elem' onChange={event => {
                    setPoints(event.target.value);
                    question.points = event.target.value;
                }} placeholder="  0 points" required={true}/>
            </div>
            <div className='container mt-2'>
                <div className='row'>
                    <div className='col col-12'>
                        <textarea id='q-text' minLength='2' value={inputValue} className='question-elem' rows={3}
                                  onChange={event => {
                                      setInputValue(event.target.value);
                                      question.text = event.target.value;
                                  }} placeholder="  Enter question" required={true}/>
                    </div>
                </div>
            </div>
            <div id='popup' className='popup'>
            </div>
            <div className='container'>
                <div className='row'>
                    <div className='col col-12'>
                        {selectedType === TEXT ?
                            <textarea id='q-option' placeholder="  Enter the right answer"
                                      value={question.answerOptions[0]?.text} rows={3} className='question-elem'
                                      onChange={event => handleChange(0, event.target)} required={true}/>
                            :
                            <Col>
                                <ul id='quiz-questions'>
                                    {answers.map((answer, index) =>
                                        <li key={index} className="row align-items-center">
                                            <input type="text" value={answers[index].text}
                                                   onChange={event => handleChange(index, event.target)}
                                                   className='question-elem mt-1 mr-2'
                                                   placeholder="  Answer option"/>
                                            is it right answer?
                                            {selectedType === SINGLE_RIGHT ?
                                                <input type="radio" className='question-elem ml-3'
                                                       value={answers[index].text} onChange={event => {
                                                    handleChange(index, event.target)
                                                    setRightSelected(event.target.value)
                                                }}
                                                       checked={answers[index].text.length > 0 && answers[index].text === rightSelected}
                                                       required={true}/>
                                                :
                                                <input type="checkbox" className='question-elem ml-3'
                                                       value={answers[index].text} onChange={event => {
                                                    handleChange(index, event.target)
                                                }}/>
                                            }
                                            <i className='delete-option ml-2' onClick={() => deleteAnswer(index)}> ✖ </i>
                                        </li>
                                    )}
                                </ul>
                                <Button variant='primary' className='btn-btn plus my-3' onClick={addAnswer}>+</Button>
                            </Col>
                        }
                    </div>
                </div>
            </div>
        </Card>
    )
});