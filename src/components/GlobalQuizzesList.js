import React, {useContext, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import QuizService from "../service/quiz.service";
import UserService from "../service/user.service";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import {QUIZ_ROUTE} from "../utils/consts";
import {Col, Modal, Spinner} from "react-bootstrap";

export const GlobalQuizzesList = observer(() => {

    const {globalQuizzes, user} = useContext(Context);
    const history = useHistory();
    const [quizzes, setQuizzes] = useState([]);
    const [page, setPage] = useState(globalQuizzes.page);
    
    useEffect(() => {
        const fetchData = async () => {
            try {
                const resp = await QuizService.fetchQuizzes(user.token, page, globalQuizzes.limit);
                console.log(resp)
                globalQuizzes.setQuizzes(resp.data.data);
                globalQuizzes.setTotalCount(resp.data.data.length);
                globalQuizzes.setTotalPages(resp.data.totalPages);
                setQuizzes(globalQuizzes.quizzes.filter((quiz) => quiz !== null));
            } catch (error) {
                console.error(error);
            }
        };
        fetchData()
    }, [page]);

    return (
        <Col className="ml-2 quiz-element">
            {(() => {
                if (quizzes.length > 0) {
                    return (
                        quizzes.map(quiz =>
                            <div key={quiz.id} className='row m-2 border-bottom justify-content-between' role="button"
                                 onClick={() => {
                                     if (!quiz.isSolved) {
                                         history.push(QUIZ_ROUTE + '/' + quiz.id)
                                     } else {
                                         document.getElementById('popup').style.right = '0';
                                         setTimeout(() => {
                                             if (document.getElementById('popup')) {
                                                 document.getElementById('popup').style.right = '-3000px';
                                             }
                                         }, 2000);
                                     }
                                 }}>
                                <span className='row justify-content-between'><span className='font-weight-bold mr-2'>{quiz.name} </span><span className='font-weight-light'> by {quiz.authorName}</span></span>
                                {(() => {
                                    if (quiz.points !== -1) {
                                        return (
                                            <div>{quiz.points} / {quiz.maxPoints}</div>
                                        )
                                    }
                                })()}
                            </div>
                        )
                    )
                }
            }) ()}
            {(() => {
                if (globalQuizzes.totalPages > 1) {
                    let pages = [];
                    for (let i = 0; i < globalQuizzes.totalPages - 1; i++) {
                        pages.push(i + 1);
                    }
                    return (
                        <span>
                            <ul className='pagination'>
                                <li className='page-item'>
                                    <p className='page-link' onClick={event => {
                                        if (page > 1) {
                                            setPage(prevState => prevState - 1)
                                        }
                                    }}>
                                        Previous
                                    </p>
                                </li>
                                {pages.map(page =>
                                    <li className='page-item'><p className='page-link'>${page + 1}</p></li>
                                )}
                                <li className='page-item'><p className='page-link' onClick={event => {
                                    if (page < globalQuizzes.totalPages) {
                                        setPage(prevState => prevState + 1)
                                    }
                                }}>Next</p></li>
                            </ul>
                        </span>
                    )
                }
            })()}
            <div id='popup' className='popup'>
                You already solved this quiz, try another one please
            </div>
        </Col>
    );
});