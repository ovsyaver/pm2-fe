import React, {useContext} from 'react';
import {Button, Col, Container, Nav, Navbar} from "react-bootstrap";
import {Context} from "../index";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import {AUTH_ROUTE, MAIN_ROUTE, MYQUIZ_ROUTE, NEWQUIZ_ROUTE, STAT_ROUTE, USER_ROUTE} from "../utils/consts";
import {observer} from "mobx-react-lite";

const NavBar = observer(() => {
    const {user} = useContext(Context)
    const history = useHistory();

    function logOut() {
        user.setUser({})
        user.setIsAuth(false)
        sessionStorage.removeItem('id');
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('email');
        sessionStorage.removeItem('name');
        history.push(AUTH_ROUTE)
    }

    return (
        <Navbar className={user.isAuth ? 'd-flex flex-column m-0' : 'd-none'}>
            <Container className='mt-1 mb-5'>
                <button onClick={() => history.push(NEWQUIZ_ROUTE)} className='m-0'>
                    Create New Quiz
                </button>
            </Container>
            <Col>
                <Nav variant='underline' className='d-flex flex-column'>
                    <Nav.Link variant="text" onClick = {() => history.push(MYQUIZ_ROUTE)}>My quizzes</Nav.Link>
                    <Nav.Link variant="text" onClick = {() => history.push(MAIN_ROUTE)}>Global quizzes</Nav.Link>
                    <Nav.Link variant="text" onClick = {() => history.push(STAT_ROUTE)}>Statistics</Nav.Link>
                    <Nav.Link variant="text" onClick = {() => history.push(USER_ROUTE)}>Account</Nav.Link>
                    <Nav.Link className='last-link mt-4' variant="text" onClick={() => logOut()}>Log out</Nav.Link>
                </Nav>
            </Col>
        </Navbar>
    );
});

export default NavBar;