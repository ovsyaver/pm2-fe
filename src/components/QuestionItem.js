import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite";
import {Card, Col, Form} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import {MULTIPLE_RIGHT, SINGLE_RIGHT, TEXT} from "../utils/consts";
import {Context} from "../index";

export const QuestionItem = observer(({question}) => {

    const {quiz} = useContext(Context);
    const types = [{text: 'Text Question', value:TEXT}, {text: 'Single Right Choice', value:SINGLE_RIGHT}, {text: 'Multiple Right Choices', value:MULTIPLE_RIGHT}]
    const [inputValue, setInputValue] = useState('');
    const setAnswer = (answer) => {
        let chosenAnswer = quiz.answers.get(question.id);
        switch (question.type){
            case MULTIPLE_RIGHT:
                if (chosenAnswer !== undefined){
                    chosenAnswer = [...chosenAnswer, answer];
                } else {
                    chosenAnswer = [];
                    chosenAnswer.push(answer);
                }
                break;
            default:
                chosenAnswer = [answer];
                break;
        }
        quiz.answers.set(question.id, chosenAnswer)
    }

    function deleteAnswer(answer) {
        let chosenAnswer = quiz.answers.get(question.id);
        if (chosenAnswer !== undefined){
            delete chosenAnswer[chosenAnswer.indexOf(answer)];
        }
    }

    return (
        <Card className='question-body mb-3'>
            <Col>
                <div className='row justify-content-between'>
                    <div className='col-3'>{types.map(type => {
                        if (type.value === question.type) {
                            return type.text;
                        }
                    })}</div>
                    <div className='col-2'>{question.points} {question.points > 1 ? 'points' : 'point'}</div>
                </div>
                <div className='row ml-1 mt-2'>
                    <h4>{question.text}</h4>
                </div>
            </Col>
            <Col>
                {question.type !== TEXT ?
                        question.answerOptions.map(answer =>
                            <Col key={answer.id}>
                                {question.type === SINGLE_RIGHT ?
                                    <input type="radio" value={answer.id} name={answer.id} id={answer.id}
                                           onChange={event => {
                                               setAnswer(event.target.value)
                                               setInputValue(event.target.value)
                                           }} checked={answer.id.toString() === inputValue}/>
                                    :
                                    <input type="checkbox" value={answer.id} name={answer.id} id={answer.id}
                                           onChange={event => {
                                               if (event.target.checked){
                                                   setAnswer(event.target.value)
                                               } else {
                                                   deleteAnswer(event.target.value)
                                               }
                                               setInputValue(event.target.value)
                                           }}/>
                                }
                                <label className='ml-2' htmlFor={answer.id}>{answer.text}</label>
                            </Col>
                        )
                    :
                        <input type="text" value={inputValue} onChange={event => setInputValue(event.target.value)}
                               onBlur={event => setAnswer(event.target.value)} className='mb-2'/>
                }
            </Col>
        </Card>
    );
});