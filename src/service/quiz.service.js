import axios from 'axios';

const API_URL = 'http://localhost:8080';

const LOCAL_QUIZ_API_URL = API_URL + '/api/v1/quizzes/';
const LOCAL_ANSWERS_API_URL = API_URL + '/api/v1/answers/';
const LOCAL_STATS_API_URL = API_URL + '/api/v1/statistics/';

class QuizService {
    async create(quiz, token){
        try {
            let jsonToSend = JSON.stringify(quiz)
            console.log(jsonToSend);
            return await axios.post(LOCAL_QUIZ_API_URL,
                jsonToSend,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async update(quiz, token){
        try {
            let jsonToSend = JSON.stringify(quiz)
            console.log(jsonToSend);
            return await axios.put(LOCAL_QUIZ_API_URL,
                jsonToSend,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async fetchQuiz(id, token){
        try {
            return await axios.get(LOCAL_QUIZ_API_URL + id,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async fetchMyQuizzes(token){
        try {
            return await axios.get(LOCAL_QUIZ_API_URL, {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    }
                }
            )
        } catch (err) {
            return err;
        }
    }
    async fetchSolvedQuizzes(token){
        try {
            return await axios.get(LOCAL_ANSWERS_API_URL, {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    }
                }
            )
        } catch (err) {
            return err;
        }
    }


    async fetchQuizzes(token, page, size){
        try {
            return await axios.get(LOCAL_QUIZ_API_URL + page + '/' + size,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                })
        } catch (err) {
            return err;
        }
    }

    async sendQuiz(id, token, answers){
        try {
            let jsonToSend = JSON.stringify(answers)
            return await axios.post(LOCAL_ANSWERS_API_URL + id,
                    jsonToSend,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                })
        } catch (err) {
            return err;
        }
    }

    async deleteQuiz(id, token) {
        try {
            return await axios.delete(LOCAL_QUIZ_API_URL + id,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async fetchAnswer(id, token) {
        try {
            return await axios.get(LOCAL_ANSWERS_API_URL + id,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async fetchStats(token){
        try{
            return await axios.get(LOCAL_STATS_API_URL,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

}
export default new QuizService();