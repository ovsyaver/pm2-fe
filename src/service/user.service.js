import axios from 'axios';

const API_URL = 'http://localhost:8081';
const LOCAL_API_URL = API_URL + '/users';
class UserService {
    async login(user) {
        try {
            let jsonToSend = JSON.stringify(user)

            return await axios.post(LOCAL_API_URL + '/login',
                jsonToSend,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        }
        catch(err) {
            return err;
        }
    }

    async register(user) {
        try {
            let jsonToSend = JSON.stringify(user)
            console.log(jsonToSend)
            return await axios.post(LOCAL_API_URL,
                jsonToSend,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async fetchUser(authorId, token) {
        try {
            return await axios.get(LOCAL_API_URL + '/' + authorId,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        } catch (err) {
            return err;
        }
    }

    async changePassword(userId, newPwd, token) {
        try {
            let jsonToSend = JSON.stringify(newPwd)
            return await axios.put(LOCAL_API_URL + '/' + userId + '/password',
                jsonToSend,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        }
        catch(err) {
            return err;
        }
    }

    async deleteAccount(userId, token){
        try {
            return await axios.delete(LOCAL_API_URL + '/' + userId,
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    withCredentials: false
                }
            );
        }
        catch(err) {
            return err;
        }
    }

}
export default new UserService();