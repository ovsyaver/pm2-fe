import {makeAutoObservable} from "mobx";

export default class UserStore {
    constructor() {
        this._isAuth = false
        this._id = ""
        this._user = {}
        this._token = ""
        this._email = ""
        this._username = ""
        makeAutoObservable(this)
    }
    get isAuth() {
        return this._isAuth;
    }
    setIsAuth(value) {
        this._isAuth = value;
    }

    get id() {
        return this._id;
    }

    setId(value) {
        this._id = value;
    }

    get token() {
        return this._token;
    }

    setToken(token) {
        this._token = token
    }

    get email() {
        return this._email;
    }

    setEmail(value) {
        this._email = value;
    }

    get user() {
        return this._user;
    }

    setUser(value) {
        this._user = value;
    }

    get username() {
        return this._username;
    }

    setUsername(value) {
        this._username = value;
    }
}