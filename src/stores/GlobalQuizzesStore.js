import {makeAutoObservable} from "mobx";
export default class GlobalQuizzesStore {
    constructor() {
        this._quizzes = []
        this._page = 0
        this._totalPages = 0;
        this._totalCount = 0
        this._limit = 20
        makeAutoObservable(this)
    }

    get quizzes() {
        return this._quizes;
    }


    get totalPages() {
        return this._totalPages;
    }

    setTotalPages(value) {
        this._totalPages = value;
    }

    setQuizzes(value) {
        this._quizes = value;
    }


    get page() {
        return this._page;
    }

    setPage(value) {
        this._page = value;
    }

    get totalCount() {
        return this._totalCount;
    }

    setTotalCount(value) {
        this._totalCount = value;
    }

    get limit() {
        return this._limit;
    }

    setLimit(value) {
        this._limit = value;
    }

    isEmpty(){
        return this.quizzes.length === 0
    }

}