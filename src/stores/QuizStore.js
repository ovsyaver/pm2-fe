import {makeAutoObservable} from "mobx";
export default class QuizStore {
    constructor() {
        this._name = ''
        this._id = ''
        this._quiz = {}
        this._authorId = ''
        this._questions = []
        this._questionCount = 0
        this._answers = new Map
        this._authorName = ''
        makeAutoObservable(this)
    }
    get name() {
        return this._name;
    }
    setName(value) {
        this._name = value;
    }
    get id() {
        return this._id;
    }

    get answers() {
        return this._answers;
    }

    setAnswers(map) {
        this._answers = map;
    }

    setId(value) {
        this._id = value;
    }

    get authorId() {
        return this._authorId;
    }

    setAuthorId(value) {
        this._authorId = value;
    }

    get quiz() {
        return this._quiz;
    }

    setQuiz(value) {
        this._quiz = value;
    }

    get questions() {
        return this._questions;
    }

    setQuestions(value) {
        this._questions = value;
        this._questionCount = value.length;
    }

    get questionCount() {
        return this._questionCount;
    }

    setQuestionCount(value) {
        this._questionCount = value;
    }

    get authorName() {
        return this._authorName;
    }

    set authorName(value) {
        this._authorName = value;
    }
}